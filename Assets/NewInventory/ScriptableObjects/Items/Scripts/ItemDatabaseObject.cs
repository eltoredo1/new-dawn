﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New item database", menuName = "Inventory System/Items/Database")]
public class ItemDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
{
    public ItemObject[] _items;
    public Dictionary<int, ItemObject> _getItem = new Dictionary<int, ItemObject>();

    public void OnAfterDeserialize()
    {
        for (int i = 0; i < _items.Length; i++)
        {
            _items[i].id = i;
            _getItem.Add(i, _items[i]);
        }
    }

    public void OnBeforeSerialize()
    {
        _getItem = new Dictionary<int, ItemObject>(); // Clears the dictionary before adding data to it, it's faster to do it here
    }
}
