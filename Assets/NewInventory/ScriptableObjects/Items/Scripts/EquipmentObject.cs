﻿using UnityEngine;

[CreateAssetMenu(fileName = "New equipment object", menuName = "Inventory System/Items/Equipment")]
public class EquipmentObject : ItemObject
{
    public void Awake()
    {
        _type = ItemType.Equipment;
    }
}
