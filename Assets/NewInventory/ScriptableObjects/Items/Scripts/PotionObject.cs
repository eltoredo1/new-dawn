﻿using UnityEngine;

[CreateAssetMenu(fileName = "New potion object", menuName = "Inventory System/Items/Consumable")]
public class PotionObject : ItemObject
{
    public void Awake()
    {
        _type = ItemType.Consumable;
    }
}
