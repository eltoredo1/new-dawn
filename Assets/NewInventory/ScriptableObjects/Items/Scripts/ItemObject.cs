﻿using UnityEngine;

public abstract class ItemObject : ScriptableObject
{
    public int id;
    public Sprite _uiDisplay;
    public ItemType _type;
    public ItemBuff[] _buffs;
    [TextArea(15, 20)] public string _description;

    public Item CreateItem()
    {
        Item _newItem = new Item(this);
        return _newItem;
    }
}

[System.Serializable]
public class Item
{
    public string _name;
    public int _id;
    public ItemBuff[] _buffs; // To individually put on each item script that I want to have buffs on (not potions but equipments for example)

    public Item(ItemObject item)
    {
        _name = item.name;
        _id = item.id;
        _buffs = new ItemBuff[item._buffs.Length];

        for (int i = 0; i < _buffs.Length; i++)
        {
            _buffs[i] = new ItemBuff(item._buffs[i]._min, item._buffs[i]._max)
            {
                _attribute = item._buffs[i]._attribute,
            };
        }
    }
}

[System.Serializable]
public class ItemBuff
{
    public ItemAttribute _attribute;
    public int _value;
    public int _min;
    public int _max;

    public ItemBuff(int min, int max)
    {
        _min = min;
        _max = max;
        GenerateValue();
    }

    public void GenerateValue()
    {
        _value = UnityEngine.Random.Range(_min, _max);
    }
}

public enum ItemType
{
    Consumable,
    Equipment,
    Default
}

public enum ItemAttribute
{
    Damage,
    Armor,
    Stamina,
    Strength,
    Agility,
    Intelligence,
    Luck
}