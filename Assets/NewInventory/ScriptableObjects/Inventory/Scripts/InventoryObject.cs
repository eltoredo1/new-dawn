﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New inventory", menuName = "Inventory System/Inventory")]
public class InventoryObject : ScriptableObject//, ISerializationCallbackReceiver
{
    public string _savePath;
    
    public ItemDatabaseObject _database;
    public Inventory _container;

    public void AddItem(Item item, int amount)
    {
        if (item._buffs.Length > 0)
        {
            SetFirstEmptySlot(item, amount);
            return;
        }

        for (int i = 0; i < _container._items.Length; i++)
        {
            if (_container._items[i]._id == item._id)
            {
                _container._items[i].AddAmount(amount);
                return;
            }
        }

        SetFirstEmptySlot(item, amount);
    }

    public InventorySlot SetFirstEmptySlot(Item item, int amount)
    {
        for (int i = 0; i < _container._items.Length; i++)
        {
            if (_container._items[i]._id <= -1)
            {
                _container._items[i].UpdateSlot(item._id, item, amount);
                return _container._items[i];
            }
        }

        return null; // If inventory is full
    }

    public void MoveItems(InventorySlot item1, InventorySlot item2)
    {
        InventorySlot temp = new InventorySlot(item2._id, item2._item, item2._amount);
        item2.UpdateSlot(item1._id, item1._item, item1._amount);
        item1.UpdateSlot(temp._id, temp._item, temp._amount);
    }

    public void RemoveItem(Item item)
    {
        for (int i = 0; i < _container._items.Length; i++)
        {
            if (_container._items[i]._item == item)
                _container._items[i].UpdateSlot(-1, null, 0);
        }
    }

    [ContextMenu("Save")] // So we can execute the method in the Unity Editor
    public void Save()
    {
        IFormatter _formatter = new BinaryFormatter();
        Stream _stream = new FileStream(string.Concat(Application.persistentDataPath, _savePath), FileMode.Create, FileAccess.Write);
        _formatter.Serialize(_stream, _container);
        _stream.Close();
    }

    [ContextMenu("Load")]
    public void Load()
    {
        if (File.Exists(string.Concat(Application.persistentDataPath, _savePath)))
        {
            IFormatter _formatter = new BinaryFormatter();
            Stream _stream = new FileStream(string.Concat(Application.persistentDataPath, _savePath), FileMode.Open, FileAccess.Read);
            Inventory newContainer = (Inventory)_formatter.Deserialize(_stream);

            for (int i = 0; i < _container._items.Length; i++)
            {
                _container._items[i].UpdateSlot(newContainer._items[i]._id, newContainer._items[i]._item, newContainer._items[i]._amount);
            }

            _stream.Close();
        }
    }
}

[System.Serializable]
public class Inventory
{
    public InventorySlot[] _items = new InventorySlot[30]; // Rendre dynamique
}

[System.Serializable]
public class InventorySlot
{
    public int _id = -1;
    public Item _item;
    public int _amount;
    public InventorySlot()
    {
        _id = -1;
        _item = null;
        _amount = 0;
    }

    public InventorySlot(int id, Item item, int amount)
    {
        _id = id;
        _item = item;
        _amount = amount;
    }

    public void UpdateSlot(int id, Item item, int amount)
    {
        _id = id;
        _item = item;
        _amount = amount;
    }

    public void AddAmount(int value)
    {
        _amount += value;
    }
}