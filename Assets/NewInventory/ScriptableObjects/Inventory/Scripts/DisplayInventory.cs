﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class DisplayInventory : MonoBehaviour
{
    public MouseItem _mouseItem = new MouseItem();

    public GameObject _inventoryPrefab;
    public InventoryObject _inventory;

    public int _xStart;
    public int _yStart;
    public int _xSpaceBetweenItems;
    public int _ySpaceBetweenItems;
    public int _numberOfColumns;

    private Dictionary<GameObject, InventorySlot> _itemsDisplayed = new Dictionary<GameObject, InventorySlot>();

    void Start()
    {
        CreateSlots();
    }

    void Update()
    {
        UpdateSlot(); // To call when a change is made in the inventory later on
    }

    public void UpdateSlot()
    {
        foreach (KeyValuePair<GameObject, InventorySlot> slot in _itemsDisplayed)
        {
            if (slot.Value._id >= 0)
            {
                slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().sprite = _inventory._database._getItem[slot.Value._id]._uiDisplay;
                slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 1);
                slot.Key.GetComponentInChildren<TextMeshProUGUI>().text = slot.Value._amount == 1 ? "" : slot.Value._amount.ToString("n0");
            }
            else
            {
                slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().sprite = null;
                slot.Key.transform.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0);
                slot.Key.GetComponentInChildren<TextMeshProUGUI>().text = "";
            }
        }
    }

    public void CreateSlots()
    {
        _itemsDisplayed = new Dictionary<GameObject, InventorySlot>();

        for (int i = 0; i < _inventory._container._items.Length; i++)
        {
            var obj = Instantiate(_inventoryPrefab, Vector3.zero, Quaternion.identity, transform);
            obj.GetComponent<RectTransform>().localPosition = GetPosition(i);

            AddEvent(obj, EventTriggerType.PointerEnter, delegate { OnEnter(obj); });
            AddEvent(obj, EventTriggerType.PointerExit, delegate { OnExit(obj); });
            AddEvent(obj, EventTriggerType.BeginDrag, delegate { OnDragStart(obj); });
            AddEvent(obj, EventTriggerType.EndDrag, delegate { OnDragEnd(obj); });
            AddEvent(obj, EventTriggerType.Drag, delegate { OnDrag(obj); });

            _itemsDisplayed.Add(obj, _inventory._container._items[i]);
        }
    }

    public void AddEvent(GameObject obj, EventTriggerType type, UnityAction<BaseEventData> action)
    {
        EventTrigger trigger = obj.GetComponent<EventTrigger>();
        var eventTrigger = new EventTrigger.Entry();
        eventTrigger.eventID = type;
        eventTrigger.callback.AddListener(action);
        trigger.triggers.Add(eventTrigger);
    }

    public void OnEnter(GameObject obj)
    {
        _mouseItem._hoveredObject = obj;

        if (_itemsDisplayed.ContainsKey(obj))
            _mouseItem._hoveredItem = _itemsDisplayed[obj];
    }

    public void OnExit(GameObject obj)
    {
        _mouseItem._hoveredObject = null;
        _mouseItem._hoveredItem = null;
    }

    public void OnDragStart(GameObject obj)
    {
        var mouseObject = new GameObject();
        var rt = mouseObject.AddComponent<RectTransform>();
        rt.sizeDelta = new Vector2(50, 50);
        mouseObject.transform.SetParent(transform.parent);

        if (_itemsDisplayed[obj]._id >= 0) // If there is an item in this slot
        {
            var image = mouseObject.AddComponent<Image>();
            image.sprite = _inventory._database._getItem[_itemsDisplayed[obj]._id]._uiDisplay;
            image.raycastTarget = false;
        }

        _mouseItem._obj = mouseObject;
        _mouseItem._item = _itemsDisplayed[obj];
    }

    public void OnDragEnd(GameObject obj)
    {
        if (_mouseItem._hoveredObject) // If we're trying to move an item onto a slot
        {
            _inventory.MoveItems(_itemsDisplayed[obj], _itemsDisplayed[_mouseItem._hoveredObject]);
        }
        else
        {
            _inventory.RemoveItem(_itemsDisplayed[obj]._item); // Deletes the item -> might want to create the same object and put in below the player's feet, or put a message asking if the player is sure about deleting the item
        }

        Destroy(_mouseItem._obj);
        _mouseItem._item = null;
    }

    public void OnDrag(GameObject obj)
    {
        if (_mouseItem._obj != null)
            _mouseItem._obj.GetComponent<RectTransform>().position = Input.mousePosition;
    }

    public Vector3 GetPosition(int i)
    {
        return new Vector3(_xStart + _xSpaceBetweenItems * (i % _numberOfColumns), _yStart + (- _ySpaceBetweenItems * (i / _numberOfColumns)), 0f);
    }
}

public class MouseItem
{
    public GameObject _obj;
    public InventorySlot _item;
    public InventorySlot _hoveredItem;
    public GameObject _hoveredObject;


}