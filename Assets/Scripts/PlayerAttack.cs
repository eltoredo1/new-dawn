﻿using System.Collections;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private Player _player;

    private bool _shouldAttack = false;
    private bool _isInCooldown = false;

    private void Update()
    {
        ProcessInputs();
    }

    private void ProcessInputs()
    {
        if (Input.GetMouseButtonDown(0))
            PrimaryAttack();

        if (Input.GetMouseButtonDown(1))
            SecondaryAttack();
    }

    private void PrimaryAttack()
    {
        if (_shouldAttack == false)
            StartCoroutine(ResetAttack());
        // Jouer l'animation de l'attaque
        // Si ça touche un ennemi, lui enlever de la vie
    }

    private void SecondaryAttack()
    {
        // Weapon launch for example
    }

    void OnTriggerStay2D(Collider2D trigg)
    {
        if (_shouldAttack && !_isInCooldown)
        {
            _isInCooldown = true;
            //Debug.Log("Triggered on: " + trigg.gameObject.name);
            if (trigg.gameObject.tag == "Enemy")
            {
                //trigg.gameObject.SetActive(false);
                Enemy _enemy = trigg.gameObject.GetComponent<Enemy>();
                if (_enemy != null)
                {
                    _enemy._vitality._life -= _player._stats._damages;
                    _enemy.OnDeath();
                    //Debug.Log("Enemy LP: "+_enemy._vitality._life.ToString());
                }
            }
        }
    }

    private IEnumerator ResetAttack()
    {
        _shouldAttack = true;
        Debug.Log("Is attacking but can't attack more");
        yield return new WaitForSeconds(1.5f);
        Debug.Log("Finished attacking and can attack more");
        _shouldAttack = false;
        _isInCooldown = false;
    }

    /*private void OnTriggerEnter(Collider trigg)
    {
        //Debug.Log("Triggered on: " + trigg.gameObject.name);
        if (trigg.gameObject.tag != "Player")
        {
            GameObject _ce = Instantiate(collisionEffect, transform.position, Quaternion.identity);
            Destroy(_ce, 2f);
        }
    }*/
}
