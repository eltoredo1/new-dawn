﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    Player _player;

    public struct Vitality
    {
        public int _life;
        public int _maxLife;
        public int _shield;
        public int _maxShield;

        public Vitality(int life, int maxLife, int shield, int maxShield)
        {
            _life = life;
            _maxLife = maxLife;
            _shield = shield;
            _maxShield = maxShield;
        }
    }
    public Vitality _vitality;

    public struct Stats
    {
        public int _level;
        public int _damages;

        public Stats(int level, int damages)
        {
            _level = level;
            _damages = damages;
        }
    }
    public Stats _stats;

    private void Update()
    {
    }

    public void OnDeath()
    {
        if (_vitality._life <= 0)
        {
            Destroy(gameObject);
            _player._progression._experience += 50;
            //Debug.Log("XP: "+_player._progression._experience);
        }
    }
}