﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public InventoryObject _inventory;

    private Skill _quickSpeedSkill;
    private Skill _generateShield;

    [System.Serializable]
    public struct Progression
    {
        public int _level;
        public float _experience;
        public float _maxExperience;

        public Progression(int level, float experience, float maxExperience)
        {
            _level = level;
            _experience = experience;
            _maxExperience = maxExperience;
        }
    }
    public Progression _progression;

    [System.Serializable]
    public struct Vitality
    {
        public int _life;
        public int _maxLife;
        public int _shield;
        public int _maxShield;

        public Vitality(int life, int maxLife, int shield, int maxShield)
        {
            _life = life;
            _maxLife = maxLife;
            _shield = shield;
            _maxShield = maxShield;
        }
    }
    public Vitality _vitality;

    [System.Serializable]
    public struct Stats
    {
        public int _damages;
        public int _armor;
        public int _stamina;
        public int _strength;
        public int _agility;
        public int _intelligence;
        public int _luck;
        public int _statPoints;

        public Stats(int damages, int armor, int stamina, int strength, int agility, int intelligence, int luck, int statPoints)
        {
            _damages = damages;
            _armor = armor;
            _stamina = stamina;
            _strength = strength;
            _agility = agility;
            _intelligence = intelligence;
            _luck = luck;
            _statPoints = statPoints;
        }
    }
    public Stats _stats;

    public float Experience
    {
        get { return _progression._experience; }
        set
        {
            _progression._experience = value;
            if (_progression._experience >= _progression._maxExperience)
            {
                LevelUp();
                _progression._maxExperience += 500;
                _progression._experience = 0;
            }
        }
    }

    public int Life
    {
        get { return _vitality._life; }
        set
        {
            _vitality._life = value;
            if (_vitality._life > _vitality._maxLife)
                _vitality._life = _vitality._maxLife;
        }
    }

    private void Awake()
    {
        _quickSpeedSkill = GetComponent<QuickSpeed>();
        _generateShield = GetComponent<GenerateShield>();

        _progression = new Progression(1, 0, 500);
        _vitality = new Vitality(100, 100, 0, 100);
        _stats = new Stats(10, 10, 5, 5, 0, 0, 0, 0);
    }

    private void Start()
    {
        //Debug.Log("Level: " + _progression._level);
        //Debug.Log("Experience: " + _progression._experience);
        //Debug.Log("Experience needed: " + _progression._maxExperience);
    }

    private void Update()
    {
        GameOver(); // when he loses life, check that
        ProcessInputs();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var item = collision.GetComponent<GroundItem>();
        if (item)
        {
            _inventory.AddItem(new Item(item._item), 1);
            Destroy(collision.gameObject);
        }
    }

    private void OnApplicationQuit() // To remove later
    {
        _inventory._container._items = new InventorySlot[30];
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    private void ProcessInputs()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Experience += 50;
            //Debug.Log("Level: " + _progression._level);
            //Debug.Log("Experience: " + _progression._experience);
            //Debug.Log("Experience needed: " + _progression._maxExperience);
        }

        if (Input.GetKeyDown(_quickSpeedSkill._key))
        {
            _quickSpeedSkill.UseSkill();
        }

        if (Input.GetKeyDown(_generateShield._key))
        {
            _generateShield.UseSkill();
        }
    }

    private void LevelUp()
    {
        _progression._level++;
 
        _vitality._maxLife += 20;
        _vitality._life = _vitality._maxLife;

        _stats._statPoints += 5;

        //Debug.Log("Level up!");
    }

    private void GameOver()
    {
        if (_vitality._life <= 0)
        {
            // Restart game
        }
    }
}
