﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    [SerializeField] GameObject _monsterPrefab;
    private int _randomMonster;

    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            Vector3 _spawnPosition = new Vector3(Random.Range(0, 3), Random.Range(0, 3));
            GameObject _monster = Instantiate(_monsterPrefab, _spawnPosition, Quaternion.identity);
        }
    }
}