﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateShield : Skill
{
    [SerializeField]
    private Player _player;

    public override void UseSkill()
    {
        if (_player._progression._level >= 5)
        {
            if (_canUseSkill)
            {
                //Debug.Log("Shield before using skill: " + _player._vitality._shield.ToString());
                _active = true;
                _canUseSkill = false;
                _timer = 0;
                _player._vitality._shield = 100;
                StartCoroutine(UpdateShield());
                //Debug.Log("Shield after using skill: " + _player._vitality._shield.ToString());
            }
        }
    }

    private IEnumerator UpdateShield()
    {
        yield return new WaitForSeconds(5);
        _player._vitality._shield = 0;
        _active = false;
        _timer = 0;
        //Debug.Log("Shield when skill is finished: " + _player._vitality._shield.ToString());
    }
}
