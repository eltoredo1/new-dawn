﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Enemy
{
    private Vector3 _pos1;
    private Vector3 _pos2;
    public float _speed = 1.0f;

    private void Awake()
    {
        _vitality = new Vitality(20, 100, 0, 100);
        _stats = new Stats(1, 10);

        _pos1 = new Vector3(Random.Range(0, 3), Random.Range(0, 3));
        _pos2 = new Vector3(Random.Range(0, 3), Random.Range(0, 3));
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(_pos1, _pos2, (Mathf.Sin(_speed * Time.time) + 1.0f) / 2.0f);
    }
}