﻿using UnityEngine;

public abstract class Skill : MonoBehaviour
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private string _description;
    [SerializeField]
    private int _skillLevel; // ?
    [SerializeField]
    protected float _coolDown;

    protected float _timer; // Make a method to check whether the timer has reached its end or not, get rid of booleans
    protected bool _active;
    protected bool _canUseSkill;

    [SerializeField]
    public KeyCode _key;

    private void Awake()
    {
        _timer = _coolDown;
        _active = false;
        _canUseSkill = false;
    }

    public void Update()
    {
        _timer += Time.deltaTime;
        CheckSkillUsage();
    }

    private void CheckSkillUsage()
    {
        if (_timer >= _coolDown)
        {
            _canUseSkill = true;
        }
        else
        {
            // Here you could do things that will happen when you try to use a skill before its ready (sound effect, UI, animation)
        }
    }

    public abstract void UseSkill();
    public virtual void OnSkillReset()
    {

    }
}