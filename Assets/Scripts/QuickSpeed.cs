﻿using System.Collections;
using UnityEngine;

public class QuickSpeed : Skill
{
    [SerializeField]
    private PlayerMovements _playerMovements;
    [SerializeField]
    private float _speedMultiplier;
    [SerializeField]
    private AnimationCurve _speedCurve;

    public override void UseSkill()
    {
        if (_canUseSkill)
        {
            _active = true;
            _canUseSkill = false;
            _timer = 0;
            StartCoroutine(UpdateSpeed());
        }
    }

    private IEnumerator UpdateSpeed()
    {
        while (_timer < _coolDown)
        {
            _playerMovements._baseSpeed = _speedCurve.Evaluate(_timer);
            yield return new WaitForSeconds(0.5f); // Every half of a second, it will check update the speed
        }
        _active = false;
        _timer = 0;
    }
}

/*
    private IEnumerator ResetSpeed()
    {
        yield return new WaitForSeconds(5);
        _playerMovements._baseSpeed /= _speedMultiplier;
    }
*/
